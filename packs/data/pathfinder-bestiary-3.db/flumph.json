{
    "_id": "4cfi0BksjHbFVY0A",
    "data": {
        "abilities": {
            "cha": {
                "mod": 2
            },
            "con": {
                "mod": 0
            },
            "dex": {
                "mod": 4
            },
            "int": {
                "mod": 1
            },
            "str": {
                "mod": 0
            },
            "wis": {
                "mod": 3
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 17
            },
            "allSaves": {
                "value": null
            },
            "hp": {
                "details": "",
                "max": 17,
                "temp": 0,
                "value": 17
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 8
            },
            "shield": {
                "ac": 2,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "exceptions": "",
                        "label": "Fly",
                        "type": "fly",
                        "value": "25"
                    }
                ],
                "value": "5"
            }
        },
        "details": {
            "alignment": {
                "value": "LG"
            },
            "blurb": "",
            "creatureType": "",
            "level": {
                "value": 1
            },
            "privateNotes": "",
            "publicNotes": "<p>Even among the numerous types of tentacled aberrations populating the lands of Golarion, flumphs are an anomaly. Unlike most other aberrations, flumphs' monstrous appearance belies their true nature as gentle, good-natured creatures whose self-imposed duty is to bolster the defenses of worlds across the multiverse against onslaught from the cosmic horrors of the Dark Tapestry between the stars.</p>\n<p>Flumphs are small, jellyfish-like creatures who normally float about 5 feet off the ground on a self-generated cushion of wind, directing their movements with jets of air from countless tiny pores on the surface of their bodies. With some effort, the flumph can force air through these openings in a way that roughly approximates humanoid speech, albeit in a squeaky and halting tone.</p>\n<p>Though a flumph's underside is ringed with an array of acidic spikes that they use to hunt small prey, they are nearly defenseless against most attackers and usually attempt to flee when threatened. If cornered, a flumph can emit a foul-smelling spray capable of temporarily incapacitating enemies while warning nearby allies of their presence. This olfactory defense mechanism causes flumphs to feel more kinship with terrestrial creatures than with the jellyfish to which they bear a closer physical resemblance.</p>\n<p>Flumphs hatch from pods flung into outer space from the distant flumph homeworld in a ritual known as the Seeding. Even a newly hatched flumph has an instinctive knowledge of their purpose and seeks to make a lair close to a nearby settlement, which they then adopt as their personal charge to protect. However, remaining acutely aware of both their physical limitations and the tendency of their appearance to frighten the unsuspecting, most flumphs recruit and guide from afar, revealing their true nature only to their most trusted allies.</p>\n<p>In addition to their missions, flumphs endeavor to lead upstanding lives on a daily basis. They notably draw guidance from their dreams through exhaustive interpretations and carefully maintained dream journals. Life lessons, areas in which they can improve their behavior, and portents of alien danger all come to flumphs in dreams-or at least, that's what they believe. Despite placing so much importance on dreams&nbsp;flumphs have never set out to prove their dreams are authentic messages.</p>",
            "source": {
                "value": "Pathfinder Bestiary 3"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 5
            },
            "reflex": {
                "saveDetail": "",
                "value": 9
            },
            "will": {
                "saveDetail": "",
                "value": 8
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "",
                "value": []
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "aklo",
                    "celestial",
                    "common"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "sm"
            },
            "traits": {
                "custom": "",
                "value": [
                    "aberration"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "wdoiMwQp71XmTDBR",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": []
                },
                "bonus": {
                    "value": 7
                },
                "damageRolls": {
                    "0": {
                        "damage": "1d4",
                        "damageType": "piercing"
                    }
                },
                "description": {
                    "value": "<p>[[/r 1d4 #persistent acid]] @Compendium[pf2e.conditionitems.Persistent Damage]{Persistent Acid Damage}</p>"
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "agile",
                        "finesse"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Spikes",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "RFaRYNVP3GPdcMkY",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Localize[PF2E.NPC.Abilities.Glossary.Darkvision]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "darkvision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.qCCLZhnp2HhP3Ex6"
                }
            },
            "img": "systems/pf2e/icons/default-icons/action.svg",
            "name": "Darkvision",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "XTe4xCgJVw7ZYYiN",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A flumph that is knocked @Compendium[pf2e.conditionitems.Prone]{Prone} must succeed at a <span data-pf2-check=\"flat\" data-pf2-dc=\"11\" data-pf2-label=\"Upside Down DC\" data-pf2-show-dc=\"gm\">flat check</span> or land on its back, rendering it @Compendium[pf2e.conditionitems.Flat-Footed]{Flat-Footed} and @Compendium[pf2e.conditionitems.Immobilized]{Immobilized}. An adjacent ally can Interact to right the flumph, removing both conditions.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Upside Down",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "RcQhtjlnYqR8A8J9",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>The flumph sprays a 20-foot line of foul-smelling liquid. Each creature caught in the spray must succeed at a <span data-pf2-check=\"fortitude\" data-pf2-dc=\"16\" data-pf2-traits=\"olfactory\" data-pf2-label=\"Spray Perfume DC\" data-pf2-show-dc=\"gm\">Fortitude</span> save or become @Compendium[pf2e.conditionitems.Sickened]{Sickened 1} (@Compendium[pf2e.conditionitems.Sickened]{Sickened 2} on a critical failure).</p>\n<p>The odor from the spray lingers for [[/r 1d4 #hours]]{1d4 hours} on all creatures that failed their saves. The sprayed creatures can be detected by smell at a range of 100 feet, and any creatures adjacent to them take a -2 circumstance penalty to saves against Spray Perfume or to recover from the sickened condition.</p>\n<p>The flumph can't use Spray Perfume again for [[/br 1d4 #rounds]]{1d4 rounds}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "olfactory"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/OneAction.webp",
            "name": "Spray Perfume",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "j4J8whKZAT0SgXpO",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 7
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Acrobatics",
            "sort": 500000,
            "type": "lore"
        },
        {
            "_id": "vewX6T3r92AKkxif",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 8
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Dark Tapestry Lore",
            "sort": 600000,
            "type": "lore"
        },
        {
            "_id": "OwxDKMHOge7JbGic",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 7
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Diplomacy",
            "sort": 700000,
            "type": "lore"
        },
        {
            "_id": "C0YcFGPgqqxVmuxH",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 7
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 800000,
            "type": "lore"
        }
    ],
    "name": "Flumph",
    "token": {
        "disposition": -1,
        "height": 1,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Flumph",
        "width": 1
    },
    "type": "npc"
}
