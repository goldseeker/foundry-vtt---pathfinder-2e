{
    "_id": "E5RDV3n7GnjAspQ5",
    "data": {
        "abilities": {
            "cha": {
                "mod": 2
            },
            "con": {
                "mod": 2
            },
            "dex": {
                "mod": 0
            },
            "int": {
                "mod": -2
            },
            "str": {
                "mod": 4
            },
            "wis": {
                "mod": 4
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 23
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "negative healing",
                "max": 110,
                "temp": 0,
                "value": 110
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 16
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "20"
            }
        },
        "details": {
            "alignment": {
                "value": "LE"
            },
            "blurb": "",
            "creatureType": "Undead",
            "level": {
                "value": 6
            },
            "privateNotes": "",
            "publicNotes": "<p>The majority of mummies were created by cruel and selfish masters to serve as guardians to protect their tombs from intruders. The traditional method of creating a mummy guardian is a laborious and sadistic process that begins well before the poor soul to be transformed is dead, during which the victim is ritualistically starved of nourishing food and instead fed strange spices, preservative agents, and toxins intended to quicken the desiccation of the flesh. The victim remains immobile but painfully aware during the final stages, where its now-useless entrails are extracted before it's shrouded in funerary wrappings and entombed within a necromantically ensorcelled sarcophagus to await intrusions in the potentially distant future. While it's certainly possible to use other methods to create a mummy guardian from an already-deceased body, those who seek to create these foul undead as their guardians in the afterlife often feel that such methods result in inferior undead-the pain and agony of death by mummification being an essential step in the process.</p>\n<p>Regardless of the method of their creation, mummy guardians are more than just physical shells of flesh and bone-they retain fragmented, distorted versions of their minds, with only enough memories of their living personality remaining to fuel their undead anger and jealousy of those who yet live. This burning rage only intensifies over the centuries of waiting within a crypt for the chance to actually act, and thus when most mummy guardians are awoken by tomb robbers or adventurers, they stop at nothing in pursuit of glorious slaughter.</p>",
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 14
            },
            "reflex": {
                "saveDetail": "",
                "value": 10
            },
            "will": {
                "saveDetail": "",
                "value": 16
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "death-effects",
                    "disease",
                    "paralyzed",
                    "poison",
                    "unconscious"
                ]
            },
            "dr": [],
            "dv": [
                {
                    "label": "Fire",
                    "type": "fire",
                    "value": "5"
                }
            ],
            "languages": {
                "custom": "Any One Ancient Language",
                "selected": [],
                "value": [
                    "necril"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "med"
            },
            "traits": {
                "custom": "",
                "value": [
                    "mummy",
                    "undead"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "EWqlu1saEnvsN9kN",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": [
                        "mummy-rot"
                    ]
                },
                "bonus": {
                    "value": 16
                },
                "damageRolls": {
                    "mubnate22akym2bmml8l": {
                        "damage": "2d6+7",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Fist",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "jXPcqu3BTPbRSWgw",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Localize[PF2E.NPC.Abilities.Glossary.Darkvision]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "darkvision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.qCCLZhnp2HhP3Ex6"
                }
            },
            "img": "systems/pf2e/icons/default-icons/action.svg",
            "name": "Darkvision",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "1ich4lSlipgnuhyC",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Template[type:emanation|distance:30]{30 Feet} @Compendium[pf2e.bestiary-ability-glossary-srd.Aura]{Aura}</p>\n<p>Living creatures are @Compendium[pf2e.conditionitems.Frightened]{Frightened 1} while in a mummy guardian's despair aura. They can't naturally recover from this fear while in the area but recover instantly once they leave the area. When a creature first enters the area, it must succeed at a <span data-pf2-check=\"will\" data-pf2-dc=\"22\" data-pf2-traits=\"aura,divine,emotion,enchantment,fear,incapacitation,mental\" data-pf2-label=\"Despair DC\" data-pf2-show-dc=\"gm\">Will</span> save (after taking the penalty from being frightened) or be @Compendium[pf2e.conditionitems.Paralyzed]{Paralyzed} for 1 round. The creature is then temporarily immune for 24 hours.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "aura",
                        "divine",
                        "emotion",
                        "enchantment",
                        "fear",
                        "incapacitation",
                        "mental"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Despair",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "giQPN0QzwFJKSkjx",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Localize[PF2E.NPC.Abilities.Glossary.NegativeHealing]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [
                    {
                        "key": "ActiveEffectLike",
                        "mode": "override",
                        "path": "data.attributes.hp.negativeHealing",
                        "value": true
                    }
                ],
                "slug": "negative-healing",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.TTCw5NusiSSkJU1x"
                }
            },
            "img": "systems/pf2e/icons/default-icons/action.svg",
            "name": "Negative Healing",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "kGKrPttfeXrnHGWr",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>This disease and any damage from it can't be healed until this curse is removed. A creature killed by mummy rot turns to dust and can't be resurrected except by a 7th-level <em>@Compendium[pf2e.spells-srd.Resurrect]{Resurrect}</em> ritual or similar magic.</p>\n<hr />\n<p><strong>Saving Throw</strong> <span data-pf2-check=\"fortitude\" data-pf2-traits=\"curse,disease,divine,necromancy,negative,damaging-effect\" data-pf2-label=\"Mummy Rot DC\" data-pf2-dc=\"22\" data-pf2-show-dc=\"gm\">Fortitude</span></p>\n<div data-visibility=\"gm\">\n<p><strong>Stage 1</strong> carrier with no ill effect (1 minute);</p>\n<p><strong>Stage 2</strong> [[/r {4d6}[negative]]]{4d6 negative damage} and @Compendium[pf2e.conditionitems.Stupefied]{Stupefied 1} (1 day)</p>\n</div>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "curse",
                        "disease",
                        "divine",
                        "necromancy",
                        "negative"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Mummy Rot",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "uZo2nxmX4XY1K9cs",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 15
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 600000,
            "type": "lore"
        },
        {
            "_id": "KTmcjM7MrMKt3ixr",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 11
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 700000,
            "type": "lore"
        }
    ],
    "name": "Mummy Guardian",
    "token": {
        "disposition": -1,
        "height": 1,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Mummy Guardian",
        "width": 1
    },
    "type": "npc"
}
